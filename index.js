// console.log("Hello AL ZAILO");


// ARRAY METHODS
// JS has built-in functions and methods for arrays.

// Mutator Methods

let fruits = ["apple", "orange", "kiwi", "dragonfruit"];

// push method
// will add element on the end part of the array
// SYNTAX: array.Name.push()

console.log("Current array: ");
console.log(fruits);
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("mutated array from push");
console.log(fruits);

// Adding multiple elements to  an array
fruits.push("avocado", "guava");
console.log("mutated arrayfrom push 2nd");
console.log(fruits);


// pop()
// Will remove array element to the end part of the array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array form from pop method");
console.log(fruits);


// Unshift()
// add element in the beginning of an array
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method");
console.log(fruits);

// shift()
// removes an element in the beginning pa of an array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);


// Splice()
// Simultaniously removes an element from specified index number and adds elements
// SYNTAX -> arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated arrayfrom splice method");
console.log(fruits);


// sort()
// re-arranges the array elements in alphanumeric order

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);


// reverse()
// reverse order from array elements
fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
// unlike the mutator methods, non cannot modify the array

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


// indexOf()
// will return the index number of the first matching element.
// if no element was found, JS will return -1
// SYNTAX -> arrayName.indexOf(searchValue, fromIndex)

let firstIndex = countries.indexOf("PH");
console.log("Result of index method " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of index method " + invalidCountry);


// lasIndexOff()
// returns index number of the last matching element in an array
// SYNTAX -> arrayName.lastindexOf(searchValue, fromIndex)

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method " + lastIndex);

// getiing index number starting from specified index
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method " + lastIndexStart);

// slice()
// portions/slices elements from an array and return a new array.
// SYNTAX -> arrayName.slice(startingIndex, endingIndex)

let slicedArrayA = countries.slice(2);
console.log("Result from slice")
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice")
console.log(slicedArrayB);

// slicing of  elements starting from the last element from the last element
let slicedArrayC = countries.slice(-3);
console.log("Result from slice")
console.log(slicedArrayC);

// toString()
// returns an array as a string  separated by commas
// SYNTAX -> arrayName.toString()
let stringArray = countries.toString();
console.log("result from string array");
console.log(stringArray); 

// concat()
// combines 2 arrays and returns a combine result
// SYNTAX -> arrayA.concat(arrayB)

let taskArray1 = ["drink html", "eat JS"];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["get it", "be node"];

tasks = taskArray1.concat(taskArray2);
console.log("result from concat method");
console.log(tasks);

// combining multiple arrays
console.log("result from concat method");
let allTasks = taskArray1.concat(taskArray2,taskArray3);
console.log(allTasks);

// combining arrays with elements
console.log("result from concat method");
let combinedTask = taskArray1.concat("smell express", "throw react");
console.log(combinedTask);

// join()
// returns an arra as a string separated by specified separator string.
// SYNTAX -> arrayName.join('separatorString')

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


// iteration methods

// forEach()
// similar to for loop that iterates on each array element
// SYNTAX -> aarayName.forEach(function(indivElement)){statement}

allTasks.forEach(function(task){
	console.log(task);
});


// using forEach with donditional statement

let filteredTask = [];
allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTask.push(task);
	}
});
console.log("Result of filtered task")
console.log(filteredTask);


// map()
// this is useful for performing task where mutating/changing the elements are required.
// SYNTAX -> let/const resulatArray = arrayName.map(functon(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap =numbers.map(function(number){
	return number * number
});
console.log("Original array");
console.log(numbers); //original is un affected by map()
console.log("Result from map method");
console.log(numberMap); // a new array is returned and restored in a avariable


// map() vs forEach

let numberForEach = numbers.forEach(function(number){
	return number * number;
})
console.log(numberForEach); //undefined
//forEach() loops ober all items in the arrays as map(), but forEach() does not return array.

// every()
// checks if all elements in an array meet the given condition
// SYNTAX - > let/const resultArray - arrayName.every(function(indivElement){condition})

let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Result from every method");
console.log(allValid)

// some()
// checks if atleast 1  elements in array meets the condidtion.
// SYNTAX - > let/const resultArray - arrayName.some(function(indivElement){condition})

let someValid = numbers.some(function(number){
	return(number < 2);
})
console.log("Result from some method");
console.log(someValid);

// filter()
// returns new array that contains elements which meet the given condition
// return an empty array if no elements  were found
// SYNTAX -> let/const resultArray = arrayName.filter(function(){condition})

let filterValid = numbers.filter(function(number){
	return(number < 3);
})
console.log("Result from filter method");
console.log(filterValid);

// includes()
// checks if the argument passed can be found in the array
// SYNTAX -> arrayName.includes(<argumentsToFind>)

let products = ["mouse","keyboard","laptop", "monitor"];

let productFound1 = products.includes("mouse");
console.log(productFound1); //true

let productFound2 = products.includes("headset");
console.log(productFound2); //false

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})

console.log(filteredProducts);

// reduce()
// evaluated elements from left ot right and ruduces  the array into single value/
// SYNTAX let/const resultArray = arrayName.reduce(function(accumulator, currentValue){operation})

let iteration = 0;

let reduceArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	// The operation to reduce  the array into single value

	return x + y;
})

console.log("Result of reduse method: " + reduceArray);

let list =["Hello","Again", "World"];

let reducedJoin = list.reduce(function(x, y){
	return x + " " + y;
})

console.log("Result of reduse method: " + reducedJoin);